package com.example.mytoastlibrary;


import android.content.Context;
import android.widget.Toast;

public class ToastActivity {

    public static void showToast(Context context , String message){
        Toast.makeText(context,message,Toast.LENGTH_LONG).show();
    }
}
